package docs

import (
	"gitlab.com/dvkgroup/go-rpc-extension/messages"
)

// swagger:route GET /api/1/user/profile user profileRequest
// Получение информации о текущем пользователе.
// security:
//   - Bearer: []
// responses:
//   200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in:body
	Body messages.ProfileResponse
}

// swagger:route POST /api/1/user/profile/chpass user chpassRequest
// Смена пароля.
// security:
//   - Bearer: []
// responses:
// 200: chpassResponse

// swagger:parameters chpassRequest
type chpassRequest struct {
	// in: body
	Body messages.UserChangePasswordRequest
}

// swagger:response chpassResponse
type chpassResponse struct {
	// in: body
	Body messages.UserChangePasswordResponse
}
