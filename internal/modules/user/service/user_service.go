package service

import (
	"context"
	"github.com/lib/pq"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/errors"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/tools/cryptography"
	"gitlab.com/dvkgroup/go-rpc-extension/messages"
	"gitlab.com/dvkgroup/go-rpc-extension/models"
	"gitlab.com/dvkgroup/go-rpc-user/internal/modules/user/storage"
	"go.uber.org/zap"
)

type UserService struct {
	storage storage.Userer
	logger  *zap.Logger
}

func NewUserService(storage storage.Userer, logger *zap.Logger) *UserService {
	return &UserService{storage: storage, logger: logger}
}

func (u *UserService) Create(ctx context.Context, in messages.UserCreateIn) messages.UserCreateOut {
	var dto models.UserDTO
	dto.SetName(in.Name).
		SetPhone(in.Phone).
		SetEmail(in.Email).
		SetPassword(in.Password).
		SetEmailVerified(true). // временно, пока не работает верификация по email
		SetRole(in.Role)

	userID, err := u.storage.Create(ctx, dto)
	if err != nil {
		if v, ok := err.(*pq.Error); ok && v.Code == "23505" {
			return messages.UserCreateOut{
				ErrorCode: errors.UserServiceUserAlreadyExists,
			}
		}
		return messages.UserCreateOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return messages.UserCreateOut{
		UserID: userID,
	}
}

func (u *UserService) Update(ctx context.Context, in messages.UserUpdateIn) messages.UserUpdateOut {
	panic("implement me")
}

func (u *UserService) VerifyEmail(ctx context.Context, in messages.UserVerifyEmailIn) messages.UserUpdateOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return messages.UserUpdateOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}
	dto.SetEmailVerified(true)
	err = u.storage.Update(ctx, dto)
	if err != nil {
		u.logger.Error("user: update err", zap.Error(err))
		return messages.UserUpdateOut{
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}

	return messages.UserUpdateOut{
		Success: true,
	}
}

func (u *UserService) ChangePassword(ctx context.Context, in messages.ChangePasswordIn) messages.ChangePasswordOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)

	if !cryptography.CheckPassword(dto.GetPassword(), in.OldPassword) {
		return messages.ChangePasswordOut{
			ErrorCode: errors.UserServiceChangePasswordErr,
		}
	}

	hashPass, err := cryptography.HashPassword(in.NewPassword)

	dto.SetPassword(hashPass)

	if err = u.storage.Update(ctx, dto); err != nil {
		u.logger.Error("parser: update err", zap.Error(err))
		return messages.ChangePasswordOut{
			ErrorCode: errors.UserServiceChangePasswordErr,
		}
	}

	return messages.ChangePasswordOut{
		Success:   true,
		ErrorCode: 0,
	}
}

func (u *UserService) GetByEmail(ctx context.Context, in messages.GetByEmailIn) messages.UserOut {
	userDTO, err := u.storage.GetByEmail(ctx, in.Email)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return messages.UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return messages.UserOut{
		User: &models.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.GetRole(),
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByPhone(ctx context.Context, in messages.GetByPhoneIn) messages.UserOut {
	panic("implement me")
}

func (u *UserService) GetByID(ctx context.Context, in messages.GetByIDIn) messages.UserOut {
	userDTO, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return messages.UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return messages.UserOut{
		User: &models.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.Role,
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByIDs(ctx context.Context, in messages.GetByIDsIn) messages.UsersOut {
	panic("implement me")
}
