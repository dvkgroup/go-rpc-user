package modules

import (
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	ucontroller "gitlab.com/dvkgroup/go-rpc-user/internal/modules/user/controller"
)

type Controllers struct {
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		User: userController,
	}
}
