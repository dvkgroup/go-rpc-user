package modules

import (
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	"gitlab.com/dvkgroup/go-rpc-user/internal/modules/user/service"
	"gitlab.com/dvkgroup/go-rpc-user/internal/storages"
)

type Services struct {
	User interfaces.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{
		User: service.NewUserService(storages.User, components.Logger),
	}
}
