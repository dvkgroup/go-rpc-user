package storages

import (
	"gitlab.com/dvkgroup/go-rpc-extension/db/adapter"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/cache"
	"gitlab.com/dvkgroup/go-rpc-user/internal/modules/user/storage"
)

type Storages struct {
	User storage.Userer
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User: storage.NewUserStorage(sqlAdapter, cache),
	}
}
